﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance Force

; ***** settings *****

global Source_File_Encoding, List_seperator
global CName_Latitude, CName_Longitude, CName_Altitude, CName_Time, CName_Speed
global lat_index, lon_index, alt_index, time_index, spe_index

IniRead, Source_File_Encoding, settings.ini, Settings, Source_File_Encoding, UTF-16
IniRead, List_seperator, settings.ini, Settings, List_seperator, `,

IniRead, CName_Latitude, settings.ini, Columns, CName_Latitude, Lat
IniRead, CName_Longitude, settings.ini, Columns, CName_Longitude, Lon
IniRead, CName_Altitude, settings.ini, Columns, CName_Altitude, Alt
IniRead, CName_Time, settings.ini, Columns, CName_Time, Time
IniRead, CName_Speed, settings.ini, Columns, CName_Speed, Speed

FileEncoding, %Source_File_Encoding%

; ***** main *****

get_files()
ExitApp

RETURN

; ***** loop through files *****

get_files() {

	Loop, Files, csv/*.csv
	{
	
		convert_file(A_LoopFileName)
	
	}
	
}

convert_file(file_name) {

	; Create GPX file head
	head := file_head()
	trk_begin := trk_begin(file_name)
	
	; Create GPX file body
	trkpt := ""
	
	; Loop through every line of the CSV file
	Loop, Read, csv\%file_name%
	{

		; Get columns positions after reading the first line of the CSV file
		if (A_Index == 1) {
			column_pos := get_column_positions(A_LoopReadLine)
		}
	
		; Get the first data and save than as start data (wpt)
		else if (A_Index == 2) {
			wpt := get_wpt(A_LoopReadLine, file_name)
		}	
	
		; Create all track points
		if (A_Index >= 2) {
			trkpt := trkpt . get_trkpt(A_LoopReadLine)
		}
		
	}

	; Create GPX file footer
	trk_end := trk_end()
	
	; Merge GPX variables into one
	gpx := head . wpt . trk_begin . trkpt . trk_end
	
	; Create the file
	FileDelete, gpx\%file_name%.gpx
	FileAppend, %gpx%, gpx\%file_name%.gpx, UTF-8

}

; ***** default file head *****

file_head() {

	head := "<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes"" ?>`n"
	head := head . "<gpx creator=""http://analyse-gps.com"" version=""1.0"" xmlns=""http://www.topografix.com/GPX/1/0"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd"">`n"

	RETURN head

}

; ***** get position of columns in array and save as array index numbers *****

get_column_positions(line) {
	
	Array := StrSplit(line, List_seperator)
	
	lat_index := FALSE
	lon_index := FALSE
	alt_index := FALSE
	time_index := FALSE
	spe_index := FALSE
	
	for index, element in Array
	{
		if (element==CName_Latitude) {
			lat_index := index
		} else if (element==CName_Longitude) {
			lon_index := index
		} else if (element==CName_Altitude) {
			alt_index := index
		} else if (element==CName_Time) {
			time_index := index
		} else if (element==CName_Speed) {
			spe_index := index
		}
		
	}
	
}

get_wpt(line, file_name) {

	Array := StrSplit(line, List_seperator)
	
	lat := StrReplace(Array[lat_index], " ", "")
	lon := StrReplace(Array[lon_index], " ", "")
	ele := StrReplace(Array[alt_index], " ", "")
	time_stamp := convert_time(Array[time_index])
	
	wpt := "<wpt lat=""" . lat . """ lon=""" . lon . """>`n"
	wpt := wpt . "<ele>" . ele . "</ele>`n"
	wpt := wpt . "<time>" . time_stamp . "</time>`n"
	wpt := wpt . "<name>" . file_name . "</name>`n"
	wpt := wpt . "<desc></desc>`n"
	wpt := wpt . "<sym>Waypoint</sym>`n"
	wpt := wpt . "</wpt>`n"
	
	RETURN wpt

}

trk_begin(file_name) {

	trk := "<trk>`n"
	trk := trk . "<name>" . file_name . "</name>`n"
	trk := trk . "<desc>Color:004000ff</desc>`n"
	trk := trk . "<trkseg>`n"
	
	RETURN trk
	
}

get_trkpt(line) {

	Array := StrSplit(line, List_seperator)
	
	lat := StrReplace(Array[lat_index], " ", "")
	lon := StrReplace(Array[lon_index], " ", "")
	ele := StrReplace(Array[alt_index], " ", "")
	spe := StrReplace(Array[spe_index], " ", "")
	time_stamp := convert_time(Array[time_index])
	
	if (lat = "0.000000" and lon = "0.000000") {
		RETURN ""
	}
	
	trkpt := "<trkpt lat=""" . lat . """ lon=""" . lon . """>`n"	
	trkpt := trkpt . "<ele>" . ele . "</ele>`n"
	trkpt := trkpt . "<time>" . time_stamp . "</time>`n"
	trkpt := trkpt . "<speed>" . spe . "</speed>`n"
	trkpt := trkpt . "</trkpt>`n"
	
	RETURN trkpt

}

trk_end() {

	trk := "</trkseg>`n"
	trk := trk . "</trk>`n"
	trk := trk . "</gpx>`n"

	RETURN trk
	
}

convert_time(date_time) {
	time_stamp := StrReplace(date_time, " ", "T") . "Z"
	RETURN time_stamp
}

; ***** hotkeys *****

^ESC::ExitApp