*** About ***
Version: 1
Date: 2016-08-03
Copyright: Benjamin Ziepert
Contact:csv_to_gpx_converter@analyse-gpx.com

*** Description ***

Converts CSV files to GPX files.

*** Instructions ***

Settings:
- You can cancel the program with Ctrl+ESC.
- You can change the settings in the settings.ini

CSV files:
- The CSV file needs the column names in the first row.
- The time schould look like this 2005-08-31 15:16:31 (yyyy-mm-dd hh:MM:ss)
- You can change the column names in the settings.ini file
- The CSV files should look like the following example:

Time;Lat;Lon;Alt;Speed;Direction
2016-06-19 11:28:25;52.31450;4.93580;0;0.00;0
2016-06-19 11:28:38;52.31450;4.93580;0;0.00;0
2016-06-19 11:32:03;52.31450;4.93580;0;0.00;0

Steps:
1. Place CSV files in the folder "csv"
2. Run CSV_to_GPX_converter.exe
3. The converted files are stored in the folder "gpx"